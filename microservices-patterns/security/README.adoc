
== Keycloak configuration

[source]
docker run -d -p 8080:8080 -e KEYCLOAK_USER=admin -e KEYCLOAK_PASSWORD=admin --name keycloak jboss/keycloak

.Realms, user and groups

* In Keycloak we can setup different reamls. A realm contains users and groups.
* By creating multiple realms we can use the same Keycloak installation for several applications

{zwsp}

. Create Realm:
http://localhost:8080/auth/admin/master/console/#/create/realm
+
image::images/README-73be7.png[]

. Create users (`user = password`, `token = password`)
http://localhost:8080/auth/admin/master/console/#/create/user/security-examples
+
image::images/README-b1340.png[]

. Create client application (`demo`)
+
image::images/README-6b46f.png[]

. Adding attributes
+
image::images/README-713e8.png[]

== Keys

Key > Realm settings > Keys

http://localhost:8080/auth/admin/master/console/#/realms/security-examples/keys

[source]
.Public key
----
-----BEGIN PUBLIC KEY-----
MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAs7vMhDUbFu5zG8gokmqjeObFl+Gz5DKZjvHmDbZ6rXoW0ih/1pjpTq00cPLPJfnto/A7364WYTb3x0iPbjR93k6ns76JP5Pm9QDuJwCzi7SzyRMhFP4ouug7Ls55sY08rOGbPD8xOUilDSe+3JpbRQeQgmYrPToUsCP+yt4nnGMEu5OpNEyYeKeUpWbHihy0HC9yn/nCR1NE4Ydn2yvPmGSJH137SmqVh8mpMCs/OA+5xg1iI1akc08jtFaz8YhCFp21t8u3ruc/JDLi+P4XFvj1dVGSRzDaZzTO+T2YYwKz80XPHA173ao/iBfq5Bba/RvTP5rH+zvMURnnSJri1QIDAQAB
-----END PUBLIC KEY-----
----

[source]
.Certificate
----
-----BEGIN RSA PRIVATE KEY-----
MIICsTCCAZkCBgFaV9NH7zANBgkqhkiG9w0BAQsFADAcMRowGAYDVQQDDBFzZWN1cml0eS1leGFtcGxlczAeFw0xNzAyMTkxOTIwNDhaFw0yNzAyMTkxOTIyMjhaMBwxGjAYBgNVBAMMEXNlY3VyaXR5LWV4YW1wbGVzMIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAs7vMhDUbFu5zG8gokmqjeObFl+Gz5DKZjvHmDbZ6rXoW0ih/1pjpTq00cPLPJfnto/A7364WYTb3x0iPbjR93k6ns76JP5Pm9QDuJwCzi7SzyRMhFP4ouug7Ls55sY08rOGbPD8xOUilDSe+3JpbRQeQgmYrPToUsCP+yt4nnGMEu5OpNEyYeKeUpWbHihy0HC9yn/nCR1NE4Ydn2yvPmGSJH137SmqVh8mpMCs/OA+5xg1iI1akc08jtFaz8YhCFp21t8u3ruc/JDLi+P4XFvj1dVGSRzDaZzTO+T2YYwKz80XPHA173ao/iBfq5Bba/RvTP5rH+zvMURnnSJri1QIDAQABMA0GCSqGSIb3DQEBCwUAA4IBAQAowyY7QW7x0zOy5lkjmjEQtLCN8HDt7OSOkTQ3w9WcncvdKUvnvB3/S9XlCdC0qcHjGCwD/mMVaMtUn450LiL+SEvlCbp3WBo0Gj2Q0oYJ/qovT4rE1IUnyt0OFZ0FH5SKVXapWg7FjDGdwQp06dTJBj7n/5W8KZUM69/ivR0OpC/EewJhzrWHvQukgOoC7uNnkBQNHIoj8uTyQLbvta4S1pUQGEK25e6YX71w3lN7/JuGaHYdNs9gO8xJZjmceTKsBMwbdFm2lVjcznYZdwi/93d63PE/r+lDPMdXBLoo+bNsRUaH5SANQdCm1bpLkfnS9ANTTCXfpHDdXEMrM6ux
-----END RSA PRIVATE KEY-----
----

== Using token

[source]
curl --data "grant_type=password&client_id={application_name}&username={username}&password={password}" http://localhost:8080/auth/realms/{realmname}/protocol/openid-connect/token

[source]
TOKEN=`curl --data "grant_type=password&client_id=demo&username=token&password=password" http://localhost:8080/auth/realms/security-examples/protocol/openid-connect/token | sed 's/.*id_token":"//g' | sed 's/".*//g'`

== Examples

* JWT / Angular 2
+
http://paulbakker.io/java/jwt-keycloak-angular2/


// JWS verification
// https://connect2id.com/products/nimbus-jose-jwt/examples/jwt-with-rsa-signature
// https://www.jsonwebtoken.io/
