package com.example.security.jwt;

import io.vertx.core.Vertx;
import io.vertx.core.http.HttpServer;
import io.vertx.core.json.JsonObject;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import io.vertx.ext.auth.User;
import io.vertx.ext.auth.jwt.JWTAuth;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.RoutingContext;
import io.vertx.ext.web.handler.JWTAuthHandler;

public class Main {

    private final static Logger log = LoggerFactory.getLogger(Main.class);

    public static void main(String[] args) {
        Vertx vertx = Vertx.vertx();

        Router router = Router.router(vertx);

        JsonObject config = new JsonObject()
                .put("public-key", "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAnB/0wbsAXMwVZrXlgsLIqipdaU57mImfYsVgqAm3oyVjk4lotvQQdCIQzv4QXY4A6CfQI+ZakBf6oOMQaNitHLQCJdcDOSJlGg4SvccdjcniTCkSOkLPazjYIMfsoWT23tofA800J/rP3IgDpd9VObpZde0DQ7yHpLPFIfXKcHM8J2N4Yux//uKCFgD9VIOw1vaaWez9v7fBkkXSF4uMDftj6CSwRgEc76wdw/c1iZP0fxQ3/K2Qvrbf3g1ZnPgLpnq1haUra4EdAxm7+mX0WcX3OlxHu9UfuEa/xTg396Ggt7J6zpg+jHqeHdD80on7ZXP/2OQwza4SZvAJh5IyYQIDAQAB")
                .put("permissionsClaimKey", "realm_access/roles");

        JWTAuth authProvider = JWTAuth.create(vertx, config);
        router.route("/private/*").handler(JWTAuthHandler.create(authProvider));
        router.route("/private/*").handler(Main::logUser);

        router.route("/private/msg").handler(rc -> rc.response().end("This is private msg"));
        router.route("/public/*").handler(rc -> rc.response().end("This is public message"));

        HttpServer httpServer = vertx.createHttpServer().requestHandler(router::accept).listen(8000);
        log.info("Server started: " + httpServer.actualPort());
    }

    private static void logUser(RoutingContext rc) {
        User user = rc.user();
        if (user != null) {
            JsonObject principal = user.principal();
            log.info("Request by " + principal.getString("preferred_username"));
            log.info("Full details: " + principal.getString("given_name") + " " + principal.getString("family_name") + " [" + principal.getString("email") + "]");

            log.info("Authenticated by " + principal.getString("auth_type"));
        } else {
            log.error("No logged in user");
        }

        rc.next();
    }

}
