package com.example.web;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class VoidController {

    @GetMapping("/index.html")
    public ModelAndView index() {
        System.out.println("Public method");
        ModelAndView model = new ModelAndView("index");
        model.addObject("time", System.currentTimeMillis());
        return model;
    }

    @GetMapping("/private")
    @ResponseBody
    public String secured() {
        System.out.println("Private method");
        return "private message";
    }

}
