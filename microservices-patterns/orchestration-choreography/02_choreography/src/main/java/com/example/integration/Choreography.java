package com.example.integration;

import com.example.integration.dictionary.DictionaryWord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.cloud.stream.annotation.StreamListener;
import org.springframework.cloud.stream.messaging.Processor;
import org.springframework.cloud.stream.messaging.Sink;
import org.springframework.cloud.stream.messaging.Source;
import org.springframework.integration.support.MessageBuilder;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.stereotype.Component;

import java.util.HashMap;

@EnableBinding(Processor.class)
@SpringBootApplication
@EnableAutoConfiguration
public class Choreography implements CommandLineRunner {

    final HashMap<String, DictionaryWord> buffer = new HashMap<>();

    @Autowired
    Sender sender;

    public static void main(String[] args) {
        new SpringApplicationBuilder(Choreography.class)
                .web(false)
                .run(args);
    }

    @Override
    public void run(String... args) throws Exception {
        final String phrase = args[0];
        sender.process(phrase);
    }

    //TODO: this consumes all messages, while in choreography example only first was interesting
    //      how this can be implemented with streams (to suppress the second translation of englishWord)
    //      java.util.HashMap is not a bad choice afterall (HashMap.computeIfAbsent())
    @StreamListener(Processor.INPUT)
    public void handle(DictionaryWord w) {
        buffer.computeIfAbsent(w.englishWord, s -> {
            System.out.println("w = " + w);
            return w;
        });
    }

}

@Component
class Sender {


    private final Source source;

    @Autowired
    public Sender(Source source) {
        this.source = source;
    }

    public void process(String word) {
        source.output().send(MessageBuilder.withPayload(word).build());
    }
}

