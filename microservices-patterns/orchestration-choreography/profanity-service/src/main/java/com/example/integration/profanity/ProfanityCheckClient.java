package com.example.integration.profanity;

public interface ProfanityCheckClient {

    boolean isObscenityWord(String word);

}
