package com.example.integration;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.Optional;
import java.util.concurrent.CompletableFuture;
import java.util.function.Function;

@SpringBootApplication
@EnableAutoConfiguration
public class Orchestration {

    @Bean
    RestTemplate restTemplate() {
        return new RestTemplate();
    }

    public static void main(String[] args) {
        new SpringApplicationBuilder(Orchestration.class)
                .web(false)
                .run(args);
    }

}

@Component
class Runner implements CommandLineRunner {

    @Autowired
    ProfanityClient profanity;

    @Autowired
    DictClient dict;

    @Autowired
    BablaClient babla;

    @Override
    public void run(String... args) throws Exception {
        final String phrase = args[0];

        CompletableFuture<Boolean> isObscene = CompletableFuture
                .supplyAsync(() -> profanity.checkProfanity(phrase));

        CompletableFuture<Optional<DictionaryWord>> noTranslation =
                CompletableFuture.completedFuture(Optional.empty());

        CompletableFuture<Optional<DictionaryWord>> maybeDictTranslation =
                CompletableFuture.supplyAsync(() -> dict.translate(phrase));

//        CompletableFuture<Optional<DictionaryWord>> maybeBablaTranslation =
//                CompletableFuture.supplyAsync(() -> babla.translate(phrase));

//        CompletableFuture<Optional<DictionaryWord>> takeFirst =
//                maybeDictTranslation.applyToEither(maybeBablaTranslation, Function.identity());

        CompletableFuture<Optional<DictionaryWord>> future =
                isObscene.thenCompose(b -> b ? noTranslation: maybeDictTranslation);

        // maybe the second call isn't made
//        CompletableFuture<Optional<DictionaryWord>> future =
//                CompletableFuture.supplyAsync(() -> profanity.checkProfanity(phrase))
//                        .thenCompose(b -> b ? CompletableFuture.completedFuture(Optional.empty()) :
//                                CompletableFuture.supplyAsync(() -> dict.translate(phrase))
//                                        .applyToEither(CompletableFuture.supplyAsync(() -> babla.translate(phrase)),
//                                                Function.identity())
//                        );

        Optional<DictionaryWord> word = future.join();
        System.out.println("translate = " + word);
    }
}

@Component
class ProfanityClient {

    @Autowired
    RestTemplate restTemplate;

    public Boolean checkProfanity(String phrase) {
        final String url = "http://localhost:9980/profanity/{phrase}";

        UriComponents uri = UriComponentsBuilder.fromUriString(url)
                .buildAndExpand(phrase);
        ResponseEntity<String> entity = restTemplate.getForEntity(uri.toUri(), String.class);
        return Boolean.valueOf(entity.getBody());
    }

}


@Component
class DictClient {

    @Autowired
    RestTemplate restTemplate;

    public Optional<DictionaryWord> translate(String phrase) {
        final String url = "http://localhost:9990/translate/{phrase}";

        UriComponents uri = UriComponentsBuilder.fromUriString(url)
                .buildAndExpand(phrase);

        try {
            ResponseEntity<DictionaryWord> entity = restTemplate.getForEntity(uri.toUri(), DictionaryWord.class);
            return Optional.of(entity.getBody());
        } catch (Exception e) {
            return Optional.empty();
        }
    }

}


@Component
class BablaClient {

    @Autowired
    RestTemplate restTemplate;

    public Optional<DictionaryWord> translate(String phrase) {
        final String url = "http://localhost:9995/translate/{phrase}";

        UriComponents uri = UriComponentsBuilder.fromUriString(url)
                .buildAndExpand(phrase);
        try {
            ResponseEntity<DictionaryWord> entity = restTemplate.getForEntity(uri.toUri(), DictionaryWord.class);
            return Optional.of(entity.getBody());
        } catch (Exception e) {
            return Optional.empty();
        }
    }

}