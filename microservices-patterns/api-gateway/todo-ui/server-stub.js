var express = require('express')
var path = require('path')
var bodyParser = require('body-parser')
var app = express()

app.use(bodyParser.json())
app.use(express.static(path.join(__dirname, process.env.NODE_ENV || 'app')));

app.get('/banners/', function(req, res) {
  var img = new Buffer('iVBORw0KGgoAAAANSUhEUgAAAdQAAAA8CAYAAADfR0s3AAAABmJLR0QA/wD/AP+gvaeTAAAACXBIWXMAAAsTAAALEwEAmpwYAAAAB3RJTUUH4AwUFxgn/mMELAAAABl0RVh0Q29tbWVudABDcmVhdGVkIHdpdGggR0lNUFeBDhcAAAWCSURBVHja7dxPSNN/HMfx18Rl7kttOiEo66SUeOg0BCHo0iGyP9Od1iFCKAhG4CE6hXQIgsRDx+ogVAeJMqnTkmlBWCgEUQdBMegPGM7G5r6u0M/v8MORbbqtfbX1+z0fMJAP7+/7+/l+Nnjtu32myxhjBAAAylLFEgAAQKACAECgAgBAoAIAAAIVAAACFQAAAhUAAAIVAAAQqAAAEKgAABCoAAAQqAAAgEAFAIBABQCAQAUAgEAFAAAEKgAABCoAAAQqAAAEKgAAIFABACBQAQAgUAEAIFABAACBCgAAgQoAAIEKAAAIVAAACFQAAAhUAAAIVAAAQKACAECgAgBAoAIAQKACAAACFQCAyg9Ul8sll8vleO3f2gsA8P9SnS9USmGMYRUBAATqnzipkyFMoAMAKjJQ8wXU6l0r4QUAQH6Ob0pKp9Pq7u6W1+tVQ0ODLl++nBPEv35Xadu2rly5ogMHDsiyLHm9Xh05ckRPnjwpeD4ne/18DefPn1dDQ4N27NihYDCo2dnZNTXRaFQdHR3y+XzyeDxqb2/XyMjIuvMrZV2KqZWku3fv6vDhw/L5fKqpqVFTU5MuXbqkRCKRt69t24pEItq1a5eqq6t59QOAk0wRJJlCpas1oVAo+/fq4+bNmxv2O3PmTM4xxZxzs3p1dnbmHNvY2Gjm5+dzan9+uN1uMzExUfa6FKpdWVkx4XB43etsbW01iUQip29XV1dJ6wEAKJ7jd6gzMzN6+/atvn37pnPnzkmSBgYGNjzm8ePHkqS+vj7F43FlMhm9evVKnZ2dJZ/fiV6Tk5MaHR1VMplULBbTvn379PHjR12/fj1bEwqFNDk5qaWlJU1PTysYDOrHjx+6ceNG2etSqPbOnTu6f/++GhsbNTg4qK9fvyqdTuvly5cKBAJ69+6drl27ltN3fHxcT58+VTKZ5ON7AKj0O9Tx8fHs2KdPn4wkY1nWhv2am5uNJHPs2DHT29trxsbGzPLysvmduTnRa3h4eM34o0ePjCTT0tKy7rFzc3NGktm7d2/Z61Kotq2tzUgyL168yJnH9PS0kWSam5tz+j58+JC3kACwSVymiFuVYjYlrdYsLS2ppqYmW19VVZVz7K/9RkZGFA6HNTc3l61pamrSgwcPdPDgwZLm5kSvhYUF+Xy+7PjCwoLq6+u1fft22bat5eVl9ff36969e5qamlI6nc7Wbtu2TZlMpqx1KVRrWdaac+bjdrv1/fv3NX3j8bjq6up4FwkAm8DxQM230aZQoEr/biaKxWKKRqMaHBzU58+fdejQIT1//rzkuZXbq1Cg9vT0qL+/f6O7fsfWJd+4x+ORbdvFfPpQ9PMHAPiPBOrPvnz5ot27d8vj8WhxcbGsuf1Or+HhYR0/fjw7PjQ0pGAwqJaWFr1//15+v1/xeFwDAwPq6OiQ1+tVIpGQ3+/fkkANBAKamJjQ69evFQgE5MTzBwAoT0X8L9+jR48qGo1qcXFRqVRKQ0NDkqSVlZU/0isSiWhsbEypVEqjo6O6ePGiJGVDdjWYdu7cKcuy9OHDh+zmoa1w4cIFSdKJEyd0+/Ztzc7OyrZtZTIZTU1N6datW2pvb+fVDQBbyelNScWM/zqmdX7+EQ6HS56bE72CweCGP5vJ95OVU6dOFXWtpazLRuORSGTda11vTQAAm6ciAjUWi5muri7j9/tNbW2t2b9/v7l69apJp9Mlz82JXqlUynR3d5u6ujpjWZY5efKkmZmZydbNz8+b06dPm/r6euPz+czZs2dNMpnc0kA1xphnz56ZUChk9uzZY9xut6mtrTWtra2mp6fHvHnzhkAFgErb5QsAAP6C71ABACBQAQAAgQoAAIEKAACBCgAAgQoAAAhUAAAIVAAAKs4/NgPC7QsOiEcAAAAASUVORK5CYII=', 'base64')

  res.writeHead(200, {
    'Content-Type': 'image/png',
    'Content-Length': img.length
  });
  res.end(img);
})

app.get('/*', function (req, res) {
  console.log('GET req: ' + req.protocol + '://' + req.get('host') + req.originalUrl)
  res.setHeader('Content-Type', 'application/json');
  res.end(JSON.stringify(
      [
          {"id": 1, "title":"First todo","order":1,"completed":false},
          {"id": 2, "title":"Second todo - which is done","order":2,"completed":true},
          {"id": 3, "title":"Not done todo","order":3,"completed":false}
      ]
  ));
})

app.all('/*', function(req, res) {
    console.log(req.method + ' req: ' + req.protocol + '://' + req.get('host') + req.originalUrl)
    console.log("  request body: " + JSON.stringify(req.body))
    var echo = req.body
    if (req.body.id === undefined) {
      echo.id = Math.floor(Math.random() * 100) + 1
    }
    res.send(echo)
    res.status(201)
    res.end()
});

var server = app.listen(3000, function () {
  var host = server.address().address
  var port = server.address().port

  console.log('Example app listening at http://%s:%s', host, port)

})
