package com.example.apigateway.gateway;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.client.SimpleClientHttpRequestFactory;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.context.request.async.DeferredResult;
import org.springframework.web.servlet.HandlerMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Arrays;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Executor;

@SpringBootApplication
public class GatewayApp {

    /*
        // http://www.sczyh30.com/vertx-blueprint-microservice/api-gateway.html
        * Authentication
        * Failure handling
        * Simple load-balancing
        * Simple heart-beat check
    */

    public static void main(String[] args) {
        SpringApplication.run(GatewayApp.class, args);
    }

    @Bean
        //@LoadBalanced
        //CircuitBreaking
    RestTemplate rest() {
        SimpleClientHttpRequestFactory factory = new SimpleClientHttpRequestFactory();
        factory.setReadTimeout(1500);
        factory.setConnectTimeout(1500);

        return new RestTemplate(factory);
    }

    @Bean
    Executor asyncExecutor() {
        ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
        executor.setCorePoolSize(4);
        executor.setMaxPoolSize(40);
        executor.setQueueCapacity(500);
        executor.setThreadNamePrefix("TristarLookup-");
        executor.initialize();
        return executor;
    }

}

@RestController
class Controllers {

    @Autowired
    RestTemplate rest;

    @Autowired
    Executor exec;

    @RequestMapping("/banners")
    public DeferredResult<byte[]> getBanners(final HttpServletResponse response) {
        final String bannersUrl = "http://localhost:8081/";

        final DeferredResult<byte[]> result = new DeferredResult<>();
        CompletableFuture.supplyAsync(() -> rest.getForObject(bannersUrl, byte[].class), exec)
                .thenAccept(b -> {
                    if (!result.isSetOrExpired()) {
                        response.setContentType("image/png");
                        response.setContentLength(b.length);
                        result.setResult(b);
                    }
                })
                .exceptionally(t -> {
                    result.setErrorResult(t);
                    return null;
                });

        return result;

    }

    @RequestMapping("/api/**")
    public DeferredResult<String> legacyTodos(@RequestBody(required = false) String payload,
                                              final HttpServletRequest request,
                                              final HttpServletResponse response) {
        final String legacyApiUrl = "http://localhost:8080/";
        final DeferredResult<String> result = new DeferredResult<>();

        final String method = request.getMethod();
        final String path = (String) request.getAttribute(HandlerMapping.PATH_WITHIN_HANDLER_MAPPING_ATTRIBUTE);

        CompletableFuture.supplyAsync(() -> {
            HttpHeaders headers = new HttpHeaders();
            headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
            headers.setContentType(MediaType.APPLICATION_JSON);
            HttpEntity<String> entity = new HttpEntity<>(payload, headers);

            return rest.exchange(legacyApiUrl + path, HttpMethod.resolve(method), entity, String.class);
        }, exec).thenAccept(responseEntity -> {
            if (!result.isSetOrExpired()) {
                if (responseEntity.getHeaders().getContentType() != null) {
                    response.setContentType(responseEntity.getHeaders().getContentType().getType());
                }
                response.setContentLength((int) responseEntity.getHeaders().getContentLength());
                response.setStatus(responseEntity.getStatusCodeValue());
                result.setResult(responseEntity.getBody());
            }
        }).exceptionally(t -> {
                    result.setErrorResult(t);
                    return null;
                });

        return result;
    }

}