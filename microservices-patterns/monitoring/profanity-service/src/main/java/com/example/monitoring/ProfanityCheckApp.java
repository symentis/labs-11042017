package com.example.monitoring;

import com.example.monitoring.profanity.ProfanityCheckClient;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
@EnableAutoConfiguration
@EnableAspectJAutoProxy
@RestController
public class ProfanityCheckApp {

    @Autowired
    ProfanityCheckClient client;

    @RequestMapping("/profanity/{phrase}")
    public boolean checkForProfanity(@PathVariable String phrase) {
        return client.isObscenityWord(phrase);
    }

    public static void main(String[] args) {
        SpringApplication.run(ProfanityCheckApp.class, args);
    }

}

@Aspect
@Component
class LoggerComponent {

    private static Logger log = LoggerFactory.getLogger(LoggerComponent.class);

    @Around("execution(* isObscenityWord(..)) && args(phrase)")
    public Object around(ProceedingJoinPoint jp, String phrase) throws Throwable {
        final Object isObscene = jp.proceed();
        log.info("Leaving profanityCheck({}): obscene={}", phrase, isObscene);
        return isObscene;
    }

}
