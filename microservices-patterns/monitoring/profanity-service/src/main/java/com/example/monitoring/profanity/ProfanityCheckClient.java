package com.example.monitoring.profanity;

public interface ProfanityCheckClient {

    boolean isObscenityWord(String word);

}
