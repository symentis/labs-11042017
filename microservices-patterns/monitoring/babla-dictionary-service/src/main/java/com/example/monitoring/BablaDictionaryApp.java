  package  com.example.monitoring;

import com.example.monitoring.dictionary.DictionaryClient;
import com.example.monitoring.dictionary.DictionaryWord;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.sleuth.Sampler;
import org.springframework.cloud.sleuth.sampler.AlwaysSampler;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
@EnableAutoConfiguration
@EnableAspectJAutoProxy
@RestController
public class BablaDictionaryApp {

    @Autowired
    DictionaryClient client;

    @RequestMapping("/translate-babla/{phrase}")
    public ResponseEntity<DictionaryWord> getFirstTranslation(@PathVariable String phrase) {
        return client.firstTranslationFor(phrase)
                .map(d -> ResponseEntity.ok(d))
                .orElseGet(() -> new ResponseEntity<DictionaryWord>(HttpStatus.NOT_FOUND));
    }

    public static void main(String[] args) {
        SpringApplication.run(BablaDictionaryApp.class, args);
    }

}

@Aspect
@Component
class LoggerComponent {

    private static Logger log = LoggerFactory.getLogger(LoggerComponent.class);

    @Around("execution(* firstTranslationFor(..)) && args(phrase)")
    public Object around(ProceedingJoinPoint jp, String phrase) throws Throwable {
        final Object translation = jp.proceed();
        log.info("Leaving firstTranslationFor({}): {}", phrase, translation);
        return translation;
    }

}
