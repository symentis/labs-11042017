package com.example.monitoring;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.sleuth.instrument.async.LazyTraceExecutor;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.Optional;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Executors;
import java.util.function.Function;

@SpringBootApplication
@EnableAutoConfiguration
@RestController
public class DictionaryProxyApp {

    @Autowired
    DictionaryProxy runner;

    public static void main(String[] args) {
        SpringApplication.run(DictionaryProxyApp.class, args);
    }

    @RequestMapping("/translate/{phrase}")
    public ResponseEntity<String> word(@PathVariable String phrase) {
        return runner.translate(phrase)
                .map(d -> ResponseEntity.ok(d))
                .orElseGet(() -> new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    @Bean
    RestTemplate restTemplate() {
        return new RestTemplate();
    }

    @Bean
    @Autowired
    public LazyTraceExecutor executor(BeanFactory bf) {
        return new LazyTraceExecutor(bf, Executors.newFixedThreadPool(4));
    }

}

@Component
class DictionaryProxy {

    private static Logger log = LoggerFactory.getLogger(DictionaryProxy.class);

    @Autowired
    DictClient dict;

    @Autowired
    BablaClient babla;

    @Autowired
    private LazyTraceExecutor executor;

    public Optional<String> translate(String phrase) {
        log.info("Initializing translation process for: {}", phrase);

//        async
        CompletableFuture<Optional<String>> maybeDictTranslation =
                CompletableFuture.supplyAsync(() -> dict.translate(phrase), executor);

//        CompletableFuture<Optional<String>> maybeBablaTranslation =
//                CompletableFuture.supplyAsync(() -> babla.translate(phrase), executor);
//
//        CompletableFuture<Optional<String>> takeFirst =
//                maybeDictTranslation.applyToEitherAsync(maybeBablaTranslation, Function.identity(), executor);

        Optional<String> word = maybeDictTranslation.join();
        return word;
    }
}

@Component
class DictClient {

    @Autowired
    RestTemplate restTemplate;

    public Optional<String> translate(String phrase) {
        final String url = "http://localhost:9994/translate-dict/{phrase}";

        UriComponents uri = UriComponentsBuilder.fromUriString(url)
                .buildAndExpand(phrase);

        try {
            ResponseEntity<String> entity = restTemplate.getForEntity(uri.toUri(), String.class);
            return Optional.of(entity.getBody());
        } catch (Exception e) {
            return Optional.empty();
        }
    }

}


@Component
class BablaClient {

    @Autowired
    RestTemplate restTemplate;

    public Optional<String> translate(String phrase) {
        final String url = "http://localhost:9995/translate-babla/{phrase}";

        UriComponents uri = UriComponentsBuilder.fromUriString(url)
                .buildAndExpand(phrase);
        try {
            ResponseEntity<String> entity = restTemplate.getForEntity(uri.toUri(), String.class);
            return Optional.of(entity.getBody());
        } catch (Exception e) {
            return Optional.empty();
        }
    }

}