package com.example.monitoring;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.cloud.sleuth.Sampler;
import org.springframework.cloud.sleuth.SpanAccessor;
import org.springframework.cloud.sleuth.Tracer;
import org.springframework.cloud.sleuth.instrument.async.LazyTraceExecutor;
import org.springframework.cloud.sleuth.instrument.async.TraceableExecutorService;
import org.springframework.cloud.sleuth.sampler.AlwaysSampler;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.Optional;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.function.Function;

@SpringBootApplication
@EnableAutoConfiguration
@RestController
public class Orchestration {

    @Autowired
    Runner runner;

    @RequestMapping("/translate/{phrase}")
    public ResponseEntity<DictionaryWord> word(@PathVariable String phrase) {
        return runner.translate(phrase)
                .map(d -> ResponseEntity.ok(d))
                .orElseGet(() -> new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    @Bean
    RestTemplate restTemplate() {
        return new RestTemplate();
    }

    @Bean
    public Sampler defaultSampler() {
        return new AlwaysSampler();
    }

    @Bean
    @Autowired
    public LazyTraceExecutor executor(BeanFactory bf) {
        return new LazyTraceExecutor(bf, Executors.newFixedThreadPool(4));
    }

    public static void main(String[] args) {
        SpringApplication.run(Orchestration.class, args);
    }

}

@Component
class Runner {

    private static Logger log = LoggerFactory.getLogger(Runner.class);

    @Autowired
    ProfanityClient profanity;

    @Autowired
    DictionaryClient dict;

    @Autowired
    private LazyTraceExecutor executor;

    public Optional<DictionaryWord> translate(String phrase) {
        log.info("Initializing translation process for: {}", phrase);

//        async
//        CompletableFuture<Boolean> isObscene = CompletableFuture
//                .supplyAsync(() -> profanity.checkProfanity(phrase), executor);
//
//        CompletableFuture<Optional<DictionaryWord>> noTranslation =
//                CompletableFuture.completedFuture(Optional.empty());
//
//        CompletableFuture<Optional<DictionaryWord>> maybeTranslation =
//                CompletableFuture.supplyAsync(() -> dict.translate(phrase), executor);
//
//        CompletableFuture<Optional<DictionaryWord>> future =
//                isObscene.thenComposeAsync(b -> b ? noTranslation : maybeTranslation, executor);
//
//        Optional<DictionaryWord> word = future.join();
//        return word;

//        sync
        if (!profanity.checkProfanity(phrase)) {
            return dict.translate(phrase);
        } else {
            return Optional.empty();
        }
    }
}

@Component
class ProfanityClient {

    @Autowired
    RestTemplate restTemplate;

    public Boolean checkProfanity(String phrase) {
        final String url = "http://localhost:9980/profanity/{phrase}";

        UriComponents uri = UriComponentsBuilder.fromUriString(url)
                .buildAndExpand(phrase);
        ResponseEntity<String> entity = restTemplate.getForEntity(uri.toUri(), String.class);
        return Boolean.valueOf(entity.getBody());
    }

}


@Component
class DictionaryClient {

    @Autowired
    RestTemplate restTemplate;

    public Optional<DictionaryWord> translate(String phrase) {
        final String url = "http://localhost:9990/translate/{phrase}";

        UriComponents uri = UriComponentsBuilder.fromUriString(url)
                .buildAndExpand(phrase);

        try {
            ResponseEntity<DictionaryWord> entity = restTemplate.getForEntity(uri.toUri(), DictionaryWord.class);
            return Optional.of(entity.getBody());
        } catch (Exception e) {
            return Optional.empty();
        }
    }

}
